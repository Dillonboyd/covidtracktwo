class HistoricDataGetter {
  gitData(){
    fetch("https://covidtracking.com/api/v1/states/daily.json")
    .then((data)=>{
      return data.json()
    }).then((data)=>{
      data = data.filter(function(item){
        return item.state == 'KS';
      });
      console.log(data);
    });
  }
}

module.exports = {HistoricDataGetter}
