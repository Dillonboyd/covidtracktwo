import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {HistoricDataGetter} from './src/historicData.tsx';
import PureChart from 'react-native-pure-chart';

var DataGetter = new HistoricDataGetter();
DataGetter.gitData();

export default function App() {
  let sampleData = [30, 200, 170, 250, 10]
  return (
    <View style={styles.container}>
      <PureChart data={sampleData} type='line' />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
